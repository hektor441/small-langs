import tm
# exploration of 1D turing machines

for i in range(1, 2**12):
    b = bin(i)[2:]
    if len(b) < 12:
        b = "0"*(12 - len(b)) + b
    print(b)
    c = 0
    tm.initialize(1, 0, {})

    for symbol in range(0, 2):
        for state in range(0, 2):
            x, y, z = map(int, list(b[c::4]))
            z = (1,) if z == 0 else (-1,)
            tm.add_transition(symbol, state, x, y, z)
            c+= 1
    tm.run(100)
    
# 100110110111  0.494949494949
# 110110111010  0.515151515152