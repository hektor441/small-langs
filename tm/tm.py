

dim  = 1
tape = {}

head    = []
cur_sym = 0
cur_state = 0

# (current_symbol, current_state) : [write_symbol, next_state, direction, ...]
table = {}

def add_transition(symbol_0, state_0, symbol_1, state_1, direction, extra=[]):
    key   = (symbol_0, state_0)
    value = [symbol_1, state_1, direction]
    if len(extra) > 0:
        value.append(extra)
    
    table[key] = value

def initialize(t_dim, init_state, init_tape):
    global dim, head, cur_state, tape
    dim  = t_dim
    head = (0,) * dim
    
    cur_state = init_state
    tape = init_tape
    
    global STAT_CHANGE_DIRECTION
    STAT_CHANGE_DIRECTION = 0

def move(direction):
    global head, cur_sym
    head = tuple(map(sum, zip(head, direction)))

    if not (head in tape):
        tape[head] = 0

    cur_sym = tape[head]


STAT_CHANGE_DIRECTION = 0


def run(iterations=10):
    global cur_state, STAT_CHANGE_DIRECTION
    
    i = 0
    oldtr = 0
    while i < iterations:
        key = (cur_sym, cur_state)
        if(not key in table):
            print("!! Undefined transition ",key)
            return i
        tr  = table[key]
        
        #print(key, tr, tape)
        
        tape[head] = tr[0] # write_symbol
        cur_state  = tr[1] # next_state
        
        # has the direction changed since the last transition?
        if oldtr == 0:
            oldtr = tr[2]
        elif tr[2] != oldtr:
            STAT_CHANGE_DIRECTION += 1
            oldtr = tr[2][:]
        
        move(tr[2])        # direction
        
        
        if len(tr) == 4:
            for extra_action in tr[3]:
                try:
                    eval(extra_action)
                except:
                    print("!! Error: extra action {0} raises an exception".format(extra_action))
        if cur_state == -1:
            print("!! Halting after {0} steps".format(i))
            break
        i+= 1
    print(print_tape(), STAT_CHANGE_DIRECTION/(iterations-1))
    
    return i

def print_tape():
    l = sorted(tape.keys())
    t = []
    if dim == 1:
        for i in l:
            t.append(tape[i])
    elif dim == 2:
        for i in range(0, l):
            s = []
            for j in range(0, l):
                s.append(tape[(i, j)])
            t.append(s)
    else:
        t = tape
    return t
"""
initialize(1, 0, {(0,):1})

L = (-1, )
R = (+1, )

add_transition(0, 0,  1, 1, R)
add_transition(0, 1,  2, 0, L)
add_transition(1, 0,  1, 1, R)
add_transition(2, 1,  1, 0, R)


run(20)"""