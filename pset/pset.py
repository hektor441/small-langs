from sys import argv
from copy import deepcopy
help_str = """.::: Pure Set Language :::.
.:: Set Construction ::.
  Syntax:
  SET := <emptyset>
       | '{' SET {',' SET} '}' 
  
  .: Empty Set :.
    `{}' or `0'

  .: Tower :.    
    T(0) := {}
    T(n) := {T(n-1)}

  .: Union of Towers :.
    U(a, b, c, ...) := Union of T(a), T(b), T(c) ... 

  .: Set Theoretic Natural Numbers :.
    int(0) := {}
    int(n) := int(n-1) + { int(n-1) }

    N = {
        {}, {{}}, {{}, {{}}}, {{}, {{}}, {{}, {{}}}}, ...
    }
    Successor function
    succ(s) := s + {s}
    
    {{}}
    p = {}
    B = ${} = {{}}
  
    C = {} U {}
    
.:: Operators ::.
  .: Prefixes :.
    `$' Power Set
    `%' or `len' Cardinality
  .: Infixes :.
    `+' Set Union
    `-' Set Difference
    `|' Set Intersection
    `^' Disjoint Union
    `*' Cross Product
    `<' Inclusion
    `=' Equality
  
"""
### Set Related functions

def set_inclusion(A, B):
    # Is every element of A in B?
    if len(A) == 0: return A in B
    for x in A:
        if not x in B:
            return False
    return True

def set_equality(A, B):
    return set_inclusion(A, B) and set_inclusion(B, A)

def set_pair(A, B):
    # Set theoretic ordered pair: (A, B) := {A, {A, B}}
    if set_equality(A, B):
        return [A, [A]]
    return [A, [A, B]]

def set_union(A, B):
    # {x | x in A or x in B}
    union = []
    for x in A:
        if not x in union:
            union.append(x)
    for x in B:
        if not x in union:
            union.append(x)
    return union

def set_difference(A, B):
    # {x | x in A and not x in B}
    diff = []
    for x in A:
        if not x in B:
            diff.append(x)
    return diff

def set_cross(A, B):
    # {(x, y) | x in A and y in B}
    cross = []
    for x in A:
        for y in B:
            cross.append(set_pair(A, B))
    return cross

def set_xor(A, B):
    # Symmetric difference (A\B)U(B\A)
    return set_union(set_difference(A, B), set_difference(B, A))

def set_power_set(A):
    # {B | B subset of A}
    if len(A) == 0: return [A]
    # magic obscure algorithm
    B = deepcopy(A)
    p = B.pop()
    B = set_power_set(B)
    C = [set_union(p, [b]) for b in B]
    return set_union(B, C)
    

### Parsing necessaire

code = ""
tokn = ""
indx = 0

sym_eof = "::EOF"
sym_err = "::ERR"
sym_nil = "::NIL"
sym_red = '\033[91m'
sym_grn = '\033[92m'
sym_blu = '\033[94m'
sym_blk = '\033[0m'

has_error = False
has_debug = True

max_recur = 2

def succ():
    global tokn, indx
    
    if has_error:
        return sym_err
    
    if indx + 1 < len(code):
        indx+= 1
        tokn = code[indx]
    else:
        tokn = sym_eof
    return tokn

def expc(e):
    if tokn == e:
        return succ()
    else:
        return error("`{}' expected but `{}' found.".format(e, tokn))

def lookahead(n=1):
    if indx + n < len(code):
        return code[indx+n]
    return ""

### Tokenization
prefixes = ["$", "%"]
infixes  = ["+", "-", "*", "^", "<", "="]
special  = ["(", ")", ",", ":", "{", "}"]
symbols = prefixes + infixes + special

op_prec = {0:["<", "="], 1:["*", "^"], 2:["+", "-"]}

def separate(s):
    if len(s) == 1: return [s]
    for sym in symbols:
        s = s.replace(sym, " " + sym + " ")
    
    while "  " in s:
        s = s.replace("  ", " ")        
    
    s = s.split()
    return s

### Miscellaneous functions

def error(msg):
    global has_error
    has_error = True
    print(sym_red, "\t!!", msg, sym_blk)
    return sym_err
    
def debug(msg):
    print(sym_blu, "\t::", msg, sym_blk)

### Parsing

functions = {}
type_nil = ".NIL"
type_set = ".SET"
type_bool= ".BOOL"
type_int = ".INT"
ctype = type_nil



def parse():
    global has_error, ctype
    ret = sym_nil
    while tokn != sym_eof:
        has_error = False
        ctype = type_nil
        
        if has_debug: debug("Parsing naked expression")
        ret = parse_expression()
        
        if has_error:
            return error("^^^")
    return ret

def apply_prefix(pfx, arg):
    global ctype
    ret = sym_nil
    if ctype == type_set:
        # Prefixes only accept set type arguments 
        if pfx == "%":
            # Cardinality of arg
            ret = len(arg)
            ctype = type_int
        elif pfx == "$":
            # Power Set of arg
            ret = set_power_set(arg)
    else:
        msg = "Type Error while parsing prefix `{}'."
        error(msg.format(pfx))
        msg = "           type{} expected but type{} found."
        ret = error(msg.format(type_set, ctype))
    return ret

def apply_infix(ifx, arg1, arg2, a1type, a2type):
    global ctype
    ret = sym_nil
    
    # Infixes only accept int, int or set, set arguments
    if a1type != a2type:
        msg = "Type Error while parsing infix `{}'."
        error(msg.format(ifx))
        msg = "           type{} expected but type{} found."
        return error(msg.format(a1type, a2type))
    elif not a1type in [type_set, type_bool]:
        msg = "Type Error while parsing infix `{}'."
        error(msg.format(ifx))
        msg = "           type{} or type{} expected but type{} found."
        return error(msg.format(type_set, type_int, a1type))
    
    elif a1type == type_int:
        # Normal operations on integers 
        if   ifx == "+": ret = arg1 +  arg2
        elif ifx == "-": ret = arg1 -  arg2
        elif ifx == "*": ret = arg1 *  arg2
        elif ifx == "^": ret = arg1 ** arg2
        elif ifx == "<": ret = int(arg1 <  arg2)
        elif ifx == "=": ret = int(arg1 == arg2)
    elif a1type == type_set:
        if   ifx == "+": ret = set_union(arg1, arg2)
        elif ifx == "-": ret = set_difference(arg1, arg2)
        elif ifx == "*": ret = set_cross(arg1, arg2)
        elif ifx == "^": ret = set_xor(arg1, arg2)
        elif ifx == "<": ret = set_inclusion(arg1, arg2)
        elif ifx == "=": ret = set_equality(arg1, arg2)

    return ret

def parse_expression(n=0):
    def parse_infix(arg1, n=0):
        while tokn in infixes:    
            ifx = tokn; succ()
            if has_debug: debug("Parsing infix `{}' at level {}".format(ifx, n))
            a1type = ctype
            if ifx in op_prec[n]:
                # We are at the right precedence level, the second argument is the term to the right
                arg2 = parse_term()
                arg1 = apply_infix(ifx, arg1, arg2, a1type, ctype)
            else:
                # We need to go deeper and evaluate the operators with higher precedence to the right
                arg2 = parse_expression(n+1)
                arg1 = apply_infix(ifx, arg1, arg2, a1type, ctype)
        return arg1
    
    if has_debug: debug("Parsing term at `{}'".format(tokn))
    ret = parse_term()
        
    if has_error:
        # Error-catching protocol
        msg = "Exception occurred at token `{}'."
        return error(msg.format(tokn))

    # Infix call: <term> <ifx> <expr>     
    ret = parse_infix(ret, n)
    
    return ret

def parse_term():
    global ctype
    ret = sym_nil
    if tokn.isdigit():
        # Integer
        ret = int(tokn)
        succ()
        ctype = type_int
    elif tokn in prefixes:
        # Prefix call: prefix <term> 
        pfx = tokn; succ()    
        arg = parse_term()
        ret = apply_prefix(pfx, arg)
    elif tokn == "{":
        # Set declaration: <set>
        succ()
        ret = parse_set()
        expc("}")
        ctype = type_set
    elif tokn == "(":
        # Parenthesized expression: '(' <expr> ')' 
        succ()
        ret = parse_expression()
        expc(")")
    elif tokn in functions:
        # Function call: <label> '(' <args> ')'
        msg = "`{}' not yet implemented"
        error(msg.format(tokn))
    else:
        msg = "Unknown symbol `{}'."
        ret = error(msg.format(tokn))
    
    return ret

def parse_set():
    def parse_subsets(main_set):
        if has_debug: debug("Parsing subsets for `{}'".format(main_set))
        expc("{")
        subsets = parse_set()
        if not subsets in ret:
            main_set.append(subsets)
        expc("}")

    ret = []
    if tokn != "{":
        if has_debug: debug("Empty set detected!")
        # Empty set case
        return ret
    
    # Recursively add another set 
    parse_subsets(ret)
        
    if has_error:
        return sym_err

    while tokn == ",":
        # Add more sets through concatenation
        succ()
        if has_debug: debug("Concatenated set detected!")
        if tokn == "{":
            parse_subsets(ret)
            
        if has_error:
            return sym_err
    if has_debug: debug("Current set stack: `{}'".format(ret))
    
    return ret
    

### Input/Output

def parse_file(filename):
    # THIS SHIT IS BROKEN
    # TODO: FIX THIS SHIT
    ret = []
    try:
        ret = open(filename, "r").readlines()
        ret = list(map(separate, ret))
    except IOError:
        msg = "The file `{}' could not be found."
        return error(msg.format(filename))
    return ret

if len(argv) > 1:
    code = parse_file(argv[1])
    indx = 0
    tokn = code[indx]
    parse()
else:
    while True:
        # Interpreter loop
        g = input("> ")
        if g == "exit":
            print("Goodbye")
            break
        elif g == "help":
            print(help_str)
        elif len(g) > 0:
            code = separate(g)
            indx = 0
            tokn = code[indx]
            if has_debug:
                debug("Code separated: `{}'".format(", ".join(code)))
            g = parse()
            
            if has_debug:
                debug("")
                msg = "Resulting expression: `{}'"
                debug(msg.format(g))