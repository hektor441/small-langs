help_str = """
=== Folder Language ===

S     =: Asgn | 'eval' CExpr
Asgn  =: Name '=' Expr
Expr  =: Term <op> Expr | Term
Term  =: <int> | <literal> | '(' Expr ')'
CExpr =: Expr | Expr 'where' {<literal> '=' CExpr ','}
"""
from sys import argv

### Parsing necessaire

code = ""
tokn = ""
indx = 0

sym_eof = "%eof"
sym_err = ["%err"]
sym_red = '\033[91m'
sym_grn = '\033[92m'
sym_blu = '\033[94m'
sym_blk = '\033[0m'

has_error = False
has_debug = True

max_recur = 2

def succ():
    global tokn, indx
    
    if has_error:
        return sym_err
    
    if indx + 1 < len(code):
        indx+= 1
        tokn = code[indx]
    else:
        tokn = sym_eof
    return tokn

def expc(e):
    if tokn == e:
        return succ()
    else:
        return error("`{}' expected but `{}' found.".format(e, tokn))

def lookahead(n=1):
    if indx + n < len(code):
        return code[indx+n]
    return ""

### Tokenization

operators     = ["+", "-", "*", "/", "%", "^"]
special_chars = ["(", ")", ",", "=", ":", ";", "`"]

def separate(s):
    if len(s) == 1: return [s]
    for c in operators + special_chars:
        s = s.replace(c, " " + c + " ")
    
    while "  " in s:
        s = s.replace("  ", " ")        
    
    s = s.split()
    return s

### Miscellaneous functions

def error(msg):
    global has_error
    has_error = True
    print(sym_red, "\t!!", msg, sym_blk)
    return sym_err
    
def debug(msg):
    print(sym_blu, "\t::", msg, sym_blk)

### Parsing
## Parsing Auxiliary Functions

def is_num(s):
    return s.replace(".", "").replace("-", "").isdigit()

def is_lit(s):
    return s.isalnum() and not s[0].isdigit()

def unfold(body, n=0):
    # unfold a variable
    # let a = 3*a;    b = a + b;
    # unfold b -> (a + b) -> ((3*a) + (a + b))
    #          -> ((3*(3*a)) + ((3*a) + (a + b)))
    n_body = []
    for i in range(0, len(body)):
        if body[i] in variables:
            var_name = body[i]
            var_body, var_unknowns, var_base = variables[var_name]
                
                
            if n < max_recur:
                if len(var_body) > 1:
                    # surround the body with parentheses
                    var_body = ["("] + var_body + [")"]
                
                # then add the variable's unknown to the current list
                for x in var_unknowns:
                    if not x in cur_unknowns:
                        cur_unknowns.append(x)
            else:
                # place the default value in place of the variable body
                var_body = var_base
                # this variable is no more unknown
                if var_name in cur_unknowns:
                    cur_unknowns.remove(var_name)
                
            # replace the variable name with its body            
            debug("replacing {} of {} with body {}".format(i, "".join(body), "".join(var_body)))
            n_body+=var_body
        elif body[i] == "$":
            n_body.append(str(n))
        else:
            n_body.append(body[i])
    if n < max_recur:
        n_body = unfold(n_body, n+1)
    return n_body

    

## Actual Parsing

keywords  = ["eval", ":", "unfold", "del"]

# name : [body, unknowns, base]
variables = {}

cur_unknowns = []
cur_base = ["0"]

def parse():
    global cur_unknowns, has_error, cur_base
    ret = []
    if tokn != sym_eof:
        cur_unknowns = []
        cur_base = ["0"]
        has_error = False
        if tokn == "out":
            # Is an output call
            succ()
            ret = parse_generic()
            print("".join(ret))
            while tokn == "," and not has_error:
                succ()
                ret = parse_generic()
                print("".join(ret))
        else:
            # Is an assignment or a naked expression
            ret = parse_generic()
            
            if has_error:
                # error-catching protocol
                msg = "Error caught at token: `{}', after expression: `{}'."
                return error(msg.format(tokn, " ".join(ret)))
            
            if tokn == "=":
                succ()
                if len(ret) != 1 or not is_lit(ret[0]):
                    msg = "Invalid name `{}' for a variable"
                    return error(msg.format(" ".join(ret)))
                else:
                    ret = parse_asgn(ret[0])
        
        if not expc(sym_eof):
            return sym_err
                
    return ret

def parse_asgn(var_name):
    # Asgn := <literal> '=' Expr
    var_body = parse_generic()
    if has_error:
        # error-catching protocol
        msg = "Error caught at token: `{}', after expression: `{}'."
        return error(msg.format(tokn, " ".join(var_name)))
    
    
    if not var_name in var_body:
        # this variable is not self referential
        cur_unknowns.remove(var_name)
        
    variables[var_name] = [var_body, cur_unknowns, cur_base]

    if has_debug:
        msg = "Created variable `{}' with body `{}', unknowns [{}] and base `{}'."
        debug(msg.format(var_name, " ".join(var_body), ", ".join(cur_unknowns), " ".join(cur_base)))
        msg = "The current variables are: {}"
        debug(msg.format(variables))

    return var_body

def parse_keyword():
    global max_recur
    kwd = tokn; succ()
    ret = []
    # keywords that take a single token as input
    if kwd == "del":        
        if tokn == "*":
            variables.clear()
            if has_debug: debug("All the variables have been deleted.")
        elif tokn in variables:
            del variables[tokn]
            succ()
            if has_debug: debug("The variable `{}' have been deleted.".format(tokn))
        else:
            msg = "The literal `{}' is not a variable and thus cannot be deleted."
            return error(msg.format(tokn))
        return ret

    # keywords that take an expression as input
    arg = parse_generic()
    if has_error:
        # error-catching protocol
        msg = "Error caught at token: `{}', after expression: `{}'."
        return error(msg.format(tokn, " ".join(ret)))
    if kwd in [":", "unfold"]:
        # Unfold
        if is_num(tokn):
            max_recur = int(tokn)
            succ()
        
        if has_debug:
            debug("Unfolding... {}".format(max_recur))
        
        ret = unfold(arg)
    elif kwd == "eval":
        # Evaluate
        if cur_unknowns == []:
            try:
                ret.append(str(eval("".join(arg))))
            except:
                msg = "The Python interpreter not evaluate the expression `{}'."
                return error(msg.format(" ".join(arg)))
        else:
            msg = "Could not evaluate the expression `{}', there are still unresolved unknowns."
            error(msg.format(" ".join(arg)))
            msg = "The current unknowns are: [{}]"
            return error(msg.format(", ".join(cur_unknowns)))
    
    return ret

def parse_generic():
    global cur_base
    ret = []
    ret = parse_expression()
        
    if has_error:
        # error-catching protocol
        msg = "Error caught at token: `{}', after expression: `{}'."
        return error(msg.format(tokn, " ".join(ret)))
    
    if tokn == ";":
        # optional separator
        succ()
        
    if tokn == "where":
        # parse and apply trailing "where" expressions
        if has_debug:
            msg = "Applying a trailing where to expression `{}'."
            debug(msg.format(" ".join(ret)))
            
        ret = parse_where(ret)
    
    elif tokn == "base":
        # parse a base expression for recursion
        succ()
        if not expc("="): return sym_err
        
        p_unknowns = cur_unknowns[:]
        cur_base = parse_generic()
        
        if p_unknowns != cur_unknowns:
            # the base must not contain unresolved unknowns
            msg = "The base `{}' for expression `{}' contains unresolved unknowns [{}]."
            p_unknowns = list(u for u in p_unknowns  if u not in cur_unknowns)
            return error(msg.format(" ".join(cur_base), " ".join(ret), ", ".join(p_unknowns)))
        if has_error:
            # error-catching protocol
            msg = "Error caught at token `{}', after expression: `{}'."
            return error(msg.format(tokn, " ".join(ret)))
    return ret 

def parse_expression():
    ret = []
    if tokn in keywords:
        ret = parse_keyword()
    else:
        ret = parse_term()
    
    while tokn in operators and not has_error:
        # Expr := Term <op> Expr
        op = tokn
        succ()
        arg = parse_expression()
        ret+= [op] + arg
    
        
    if has_error:
        # error-catching protocol
        msg = "Error caught at token: `{}', after expression: `{}'."
        return error(msg.format(tokn, " ".join(ret)))
    return ret

def parse_where(expr):
    # assign variables on the fly
    # Expr where {var_name_i = var_body_i ,}
    # ===> Expr[var_name_i|var_body_i]
    succ() # keyword where
    
    while is_lit(tokn):
        if tokn in cur_unknowns:
            var_name = tokn
            succ()
            if not expc("="): break
            
            var_body = parse_expression()
            if has_error: break
            
            cur_unknowns.remove(var_name)
            
            if has_debug:
                msg = "Replacing `{}' with `{}' in expression `{}'."
                debug(msg.format(var_name, var_body, expr))
                msg = "The current unknowns are: [{}]"
                debug(msg.format(", ".join(cur_unknowns)))
                
            if len(var_body) > 1:
                # surround the body with parentheses
                var_body = ["("] + var_body + [")"]
            
            for i in range(0, len(expr)):
                if expr[i] == var_name:
                    # insert the var body into the current expression
                    expr = expr[:i] + var_body + expr[i+1:]
        else:
            msg = "The variable `{}' does not exist in expression `{}'."
            return error(msg.format(tokn, " ".join(expr)))
        
        if tokn != ",":
            break
        succ()

    return expr

def parse_term():
    ret = []
    
    c = 0
    while tokn in ["-", "+"]:
        # handle unary minus/plus
        if tokn == "-": c+= 1
        else: c-= 1
        succ()
        
    if c%2 == 1:
        ret.append("-")

    if is_num(tokn):
        # <int>
        ret += tokn
        succ()
    elif is_lit(tokn):
        # <literal>
        if tokn in variables and lookahead() == "=":
            # delete this variable if it's being redefined
            if has_debug:
                msg = "The variable `{}' is being redefined."
                debug(msg.format(tokn))
            del variables[tokn]
            
        msg = "Parsed literal `{}'"
        
            
        if tokn in variables:
            # the literal is an already defined variable
            # replace the literal with its value
            var_name = tokn
            var_body = variables[var_name][0]
            var_unknowns = variables[var_name][1]

            if len(var_body) > 1:
                # surround the body with parentheses
                var_body = ["("] + var_body + [")"]
        
            ret+= var_body            

            # then add the variable's unknown to the current list
            for x in var_unknowns:
                if not x in cur_unknowns:
                    cur_unknowns.append(x)
            
            # DEBUG STUFF
            msg+= ", a variable with body `{}' and unknowns [{}]."
            msg = msg.format(tokn, " ".join(variables[tokn][0]), ", ".join(variables[tokn][1]))
        else:
            # the literal is an unknown
            ret.append(tokn)
            if not tokn in cur_unknowns:
                msg+= ", a NEW unknown."
                cur_unknowns.append(tokn)
            else: msg+= ", an unknown."
            
            # DEBUG STUFF
            msg = msg.format(tokn)
        
        if has_debug:
            debug(msg)
            msg = "The current unknowns are now: [{}]"
            debug(msg.format(", ".join(cur_unknowns)))    
        succ()
    elif tokn == "(":
        # parenthesized expression
        succ()
        ret+= ["("] + parse_expression() + [")"]
        expc(")")
    elif tokn == "$":
        # iterator
        ret+= ["$"]
        succ()
    elif tokn == "`":
        # silent parse of a token
        succ()
        ret.append(tokn)
        succ()
    else:
        # Illegal character
        msg = "Unexpected token: `{}'"
        error(msg.format(tokn))
        
    if has_error:
        # error-catching protocol
        return sym_err
    
    return ret



### Input/Output

def parse_file(filename):
    # THIS SHIT IS BROKEN
    # TODO: FIX THIS SHIT
    ret = []
    try:
        ret = open(filename, "r").readlines()
        ret = list(map(separate, ret))
    except IOError:
        msg = "The file `{}' could not be found."
        return error(msg.format(filename))
    return ret

if len(argv) > 1:
    code = parse_file(argv[1])
    indx = 0
    tokn = code[indx]
    parse()
else:
    while True:
        # Interpreter loop
        g = input("> ")
        if g == "exit":
            print("Goodbye")
            break
        elif g == "help":
            print(help_str)
        elif len(g) > 0:
            code = separate(g)
            indx = 0
            tokn = code[indx]
            g = parse()
            if has_error:
                error("^^^")
            elif has_debug:
                debug("")
                msg = "Parsed expression: `{}'"
                debug(msg.format(" ".join(g)))